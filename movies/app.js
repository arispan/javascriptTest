var app = angular.module("OMDbApp", [])
.controller("movieSearchCtrl", function ($scope, $http, $q) {
    $scope.movieName = "home alone"; //set model by default movie name
    $scope.$watch("movieName", function () {
        getData(); //once model changed..this function will be called
    });
    function getData() {
        var getRelated = function () {
            var deferred = $q.defer();
            $http.get('https://www.omdbapi.com/?s=' + $scope.movieName + '&apikey=b0766a0b').then(function (response) {
                deferred.resolve(response);
            }).catch(function (err) {
                deferred.reject(err);
                $scope.related = []
            });
            return deferred.promise;
        };


        getRelated().then(function (res) {
            $scope.movies = res.data.Search;
			console.log($scope.movies);
        }).catch(function (err) {
            console.error(err)
        });
    }
});
